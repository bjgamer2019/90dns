# 90dns

Nintendo Switch DNS setup with BIND9/dnsmasq and nginx for blocking all (~30) Nintendo servers.

This is mostly for use with ftpd/sys-ftpd, hb-appstore and various updaters etc.

As it blocks Nintendo servers, eshop, updates, telemetry, social feature and online play will not work. People also reported that Fortnite doesn't work.

This is a LavaTech service.

We're a small team of 2 people doing our best to create useful stuff.

[LavaTech Site](https://lavatech.top) (has links to all our other services)

[LavaTech Discord](https://discord.gg/urgYG9S)

[LavaTech Patreon](https://patreon.com/aveao) (money goes directly to server fees!)

## Prebuilt setup

There's public instances hosted at:

- `163.172.141.219` (France)
- `207.246.121.77` (USA)

See "Usage on Switch" for how to use these.

**If you live in NA or SA, then use the USA one as primary and France one as secondary. If you live anywhere else, then use France one as primary and USA one as secondary.** This isn't necessary, but it might help make things be a bit faster.

Adran used to host one, but has since stopped. Thanks for hosting an instance for well over a year, Adran!

If you have Adran's instance added as your secondary, then you have nothing to fear regarding bans, as if it can't connect to DNS, it'll simply fail instead of sending your data to Nintendo. However, I do recommend switching it to an active instance.

## Usage on Switch

- Optional, but recommended: Do tests on your PC first to see if your ISP hijacks DNS (I've seen multiple reports of this so far)
- Go to System Settings on main menu
- Go to Internet tab
- Open Internet Settings
- Open Manual Setup and set up your network name, SSID and Security
- Set DNS Settings to manual, and set DNS options to either the IPs provided above (set both primary and secondary) or your own server's IP (see below for setup instructions)
- Save and then connect to wifi

If you did everything correctly, you should be on 90DNS.

### Testing DNS connectivity

You can test if you're connected to DNS on your PC and on your switch (though it's much safer on a PC, from a ban standpoint). If the tests fail, then you probably need to set up a DNS server yourself, see [the self-host guide](SELFHOST.md).

#### On PC

You only need to do one of these, not all of them.

**Simple program for Windows:** [Download 90DNSTester](https://elixi.re/i/14onbeh1.exe) and run it any way you want. It'll test the DNS automatically and tell you if it's safe or not.

**Simple script for Linux:** Your distro probably has Python installed by default. If not, install python3 or python2 with your package manager. Install dnspython (`pip install dnspython`) and simply download and run [90dnstester.py](https://gitlab.com/a/90dns/raw/master/tester/90dnstester.py). It'll test the DNS automatically and tell you if it's safe or not.

**Easy manual method:** Set your DNS to one of the IPs provided above and open [90dns.test](http://90dns.test). If you see a screen like [this](https://elixi.re/i/fnju.png), then you're connected successfully. If not, then something is blocking the DNS connection.

**Old method:** On your PC you can set your DNS to one of the IPs provided above and try pinging the IPs (`ping example.nintendo.com`). Also on linux (and also mac?), you can use dig or drill with `drill example.nintendo.com`/`dig example.nintendo.com`. If you see 127.0.0.1, then you're connected properly.

#### On Switch

##### Via homebrew

You can use [this homebrew by Nexrem](https://github.com/meganukebmp/Switch_90DNS_tester/).

##### Manually

On your Switch, you can try to go to eshop.

- If you get 2811-1007 error then you're successfully using 90DNS
- If you successfully connect to eshop then you're not using 90DNS
- If you get 2124-4007 then you're both not using 90DNS and are console banned (rip).
- If you get 2137-7403 then you're both not using 90DNS and are CDN banned (double rip).

Additional error codes that you might get if you're successfully connected to 90DNS:
- 2124-8007 when trying to login/create new account
- 2807-1007 when trying to open a video on news
- 2807-1006 when trying to view the parental controls video
- 2160-8007 when trying to test connection
- 2801-1007 when opening "Social Network Posting Settings"
- 2137-8007 when trying to update system
- "Failed to load channel information" on "News Channel Settings" or "Find Channels"

---

## Self-hosting Guide

**You don't have to self host.** You can simply use one of the IPs above and it'll have the same functionality. This is mostly for people who want to block more/less addresses or for people who have issues accessing dns servers outside of their networks due to their ISPs.

[The selfhosting guide was moved to SELFHOST.md](SELFHOST.md)

## Disclaimer

This is not guaranteed to prevent bans. It should, but I won't guarantee that.

This currently doesn't have a way of accepting reports from console, and I don't know if I'll ever add that as this is intended to be simple and plug-and-play, and that'd require patches on the device. Just make sure that you wipe reports before leaving this DNS (keep in mind that that might also mean a ban due to local logs not matching server ones).

If anyone's willing to help me with adding support for that (even if it means patches on switch), contact me.

